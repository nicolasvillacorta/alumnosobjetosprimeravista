package com.captton.escuela;

public class Alumno {
	// Atributos
	private String nombre;
	private String apellido;
	private int edad;
	private int materiasAprobadas;
	private static String colegio;
	private static int cantAlumnos;
	
	// Constructores
	public Alumno(String nom, String ape,int ed, int matAp) {
		this.nombre = nom;
		this.apellido = ape;
		this.edad = ed;
		this.materiasAprobadas = matAp;
		Alumno.cantAlumnos++;
	}
	public Alumno(String nom, String ape, int ed) {
		this.nombre = nom;
		this.apellido = ape;
		this.edad = ed;
		Alumno.cantAlumnos++;
	}
	
	public Alumno(String nom, String ape) {
		this.nombre = nom;
		this.apellido = ape;
		Alumno.cantAlumnos++;
	}
	
	public Alumno(String nom) {
		this.nombre = nom;
		Alumno.cantAlumnos++;
	}
	
	public Alumno() {
		Alumno.cantAlumnos++;
		
	}
	
	// Getters.
	public String getNombre() {
		return this.nombre;
	}
	
	public String getApellido() {
		return this.apellido;
	}

	public int getEdad() {
		return this.edad;
	}
	
	public int getMateriasAprobadas() {
		return this.materiasAprobadas;
	}
	// Setters.
	
	public void setNombre(String nom) {
		this.nombre = nom;
	}
	
	public void setApellido(String ap) {
		this.apellido = ap;
	}

	public void setEdad(int ed) {
		this.edad = ed;
	}
	
	public void setMateriasAprobadas(int matAp) {
		this.materiasAprobadas = matAp;
	}
	
	public void setMateriasAprobradas(int man, int ingles) {
		this.materiasAprobadas = man + ingles;
	}
	
	// Demas Metodos.
	public void aproboUnaMateria() {
		this.materiasAprobadas++;
	}
	public static String getColegio() {
		return colegio;
	}
	public static void setColegio(String colegio) {
		Alumno.colegio = colegio;
	}
	public static int getCantAlumnos() {
		return cantAlumnos;
	}
	public static void setCantAlumnos(int cantAlumnos) {
		Alumno.cantAlumnos = cantAlumnos;
	}

	

}
