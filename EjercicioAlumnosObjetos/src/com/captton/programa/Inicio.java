package com.captton.programa;
import com.captton.escuela.Alumno;

public class Inicio {

	public static void main(String[] args) {
		
		Alumno alu = new Alumno();
		Alumno alu1 = new Alumno("Facundo");
		Alumno alu2 = new Alumno("Nicolas", "Villacorta");
		Alumno alu3 = new Alumno("Ezequiel", "Vazquez", 24);
		Alumno alu4 = new Alumno("Matias", "Mice", 44, 21);
		
		System.out.println(alu4.getEdad());
		System.out.println(alu2.getNombre());
		alu2.setNombre("Uriel");
		System.out.println(alu2.getNombre());
		
		System.out.println(alu2.getMateriasAprobadas());
		alu2.aproboUnaMateria();
		alu2.aproboUnaMateria();
		alu2.aproboUnaMateria();
		System.out.println(alu2.getMateriasAprobadas());
		
		System.out.println(alu2.getCantAlumnos());
		Alumno alu5 = new Alumno();
		Alumno alu6 = new Alumno("Ricardo");
		System.out.println(Alumno.getCantAlumnos());
		Alumno.setColegio("Nuevo Sol");
		System.out.println(Alumno.getColegio());
		Alumno.setColegio("ORT");
		System.out.println(Alumno.getColegio());
		System.out.println(alu4.getMateriasAprobadas());
		alu4.setMateriasAprobradas(12, 3);
		System.out.println(alu4.getMateriasAprobadas());
		
	}

}
